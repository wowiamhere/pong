# Pong 

C++ v14  
SFML 2.3  
Visual Studio 2013  

2 Player game.  
1 Paddle each side.  
Counter starts from 10.  
Counter decrements each time player does not block ball  
and ball hits wall behind it.  
When counter reaches 0 player looses.  

Type 'N'/'n' to re-start game.  


**Repo**  
https://bitbucket.org/wowiamhere/pong  

**Online Portfolio**  
http://ZenCodeMaster.com

