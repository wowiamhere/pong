#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <bitset>

using namespace std;

//***********defining type to keep countdown score from OCREDIT (unsigned short int, below)
typedef bitset<7> SCORETYPE;

//**********for window size
const unsigned short int WWIDTH{ 900 }, WHEIGHT{ 700 };

//**********for ball radius, velocity
const float BRAD{ 8.f };
float BVEL{ 6.f };

//**********for side block sizes(width, height, velocity)
const float BKW{ 12.f }, BKH{ 65.f }, BKV{ 7.f };

// *********for initial credits
//**********initial credits are deducted for side eachtime the ball
//**********hits the wall behind it.
const unsigned short int OCREDIT{ 10 };

//**********for ball
struct Ball{
	sf::CircleShape ballShape;

//*********for ball velocity
	sf::Vector2f ballVel{ BVEL, BVEL };

//*********default constructor
	Ball(float x, float y){
		ballShape.setRadius(BRAD);
		ballShape.setOrigin({ BRAD, BRAD });
		ballShape.setPosition({ x, y });
		ballShape.setOutlineThickness(4);
		ballShape.setOutlineColor(sf::Color::Yellow);
		ballShape.setFillColor(sf::Color::Black);
	}

//*********moving ball around screen
//*********keeping within bounds within y coordinates(changing speed)
//*********while letting the x go and be a loss for player
//*********IF YOU DON'T BLOCK THE BALL YOU LOOSE.
	void update(){
		ballShape.move(ballVel);

		if (rightSide() > WWIDTH)
			ballVel.x = -BVEL;
		else if (leftSide() < 0)
			ballVel.x = BVEL;

//*********keeping ball within window only if 
//*********it hits the top/bottom
//*********else player looses turn
		if (topSide() < 0)
			ballVel.y = BVEL;
		else if (bottomSide() > WHEIGHT)
			ballVel.y = -BVEL;

	}

//*********obtaining coordinates of center, left, right, top, bottom of 
//*********ball up to the radius in all directions
	float x(){ return ballShape.getPosition().x; }
	float y(){ return ballShape.getPosition().y; }
	float rightSide(){ return x() + BRAD; }
	float leftSide(){ return x() - BRAD; }
	float topSide(){ return y() - BRAD; }
	float bottomSide(){ return y() + BRAD; }

};

//**********for platform to block ball
//**********2 (one on right/left)
struct Block{

	//**********for shape
	sf::RectangleShape recShape;

	//**********for block side (left/right)
	char side = ' ';

	//**********for velocity of brick when moved
	sf::Vector2f recVel;

	//**********for checking score
	//**********will be deducted in the collision f(x)
	//**********when it reaches 0 the player looses(game ends)
	SCORETYPE scr{ OCREDIT };

	//***********constructor
	Block(float x, float y, char s){

		//***********try catch for making sure the last parameter (char
		//***********is nothing other than 'R' or 'L' (right or left)
		try{
			recShape.setSize({ BKW, BKH });
			recShape.setOrigin({ BKW / 2.f, BKH / 2.f });
			recShape.setFillColor(sf::Color::Green);
			recShape.setPosition({ x, y });

			s = toupper(s);

			if (s == 'L')
				side = 'L';
			else if (s == 'R')
				side = 'R';
			else
				throw(s);

		}
		catch (char s){
			cerr << "The last parameter of Block Constructor MUST be 'R' for right or 'L' for left.";
		}
	}

	//***********coordinates of the brick
	float x(){ return recShape.getPosition().x; }
	float y(){ return recShape.getPosition().y; }
	float leftSide(){ return x() - BKW / 2.f; }
	float rightSide(){ return x() + BKW / 2.f; }
	float topSide(){ return y() - BKH / 2.f; }
	float bottomSide(){ return y() + BKH / 2.f; }

	void update(){
		recShape.move(recVel);

		//***********if its the Left paddle, to move up down use W or S
		if (side == 'L'){
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && topSide() > 0)
				recVel.y = -BKV;
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && bottomSide() < WHEIGHT)
				recVel.y = BKV;

			//***********if no other key pressed, keep paddle still
			else
				recVel.y = 0;
		}
		//***********if its the Right paddle, to move up down use arrow up or arrow down
		else if (side == 'R'){
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && topSide() > 0)
				recVel.y = -BKV;
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && bottomSide() < WHEIGHT)
				recVel.y = BKV;

			//***********if no other key pressed, keep paddle still
			else
				recVel.y = 0;
		}
	}

};

//***********pass two objects to see if they colide.
//***********Parameters: a for paddle & b for ball
template<class A1, class A2>
bool clash(A1& a, A2& b){
	if (a.side == 'L')
		return b.leftSide() <= a.rightSide() && b.rightSide() >= a.leftSide() && b.topSide() <= a.bottomSide() && b.bottomSide() >= a.topSide();
	else if (a.side == 'R')
		return b.rightSide() >= a.leftSide() && b.leftSide() <= a.rightSide() &&
		b.topSide() <= a.bottomSide() && b.bottomSide() >= a.topSide();
}

//***********pass 3 objects to see where they have colided
//***********Parameters: a for left paddle, b for ball, c for right paddle
template<class T1, class T2, class T3>
void colide(T1& a, T2& b, T3& c){
	if (clash(a, b)){
		b.ballVel.x = BVEL;

		if (b.y() <= a.y())
			b.ballVel.y = -BVEL;
		else
			b.ballVel.y = BVEL;
	}
	else if (clash(c, b)){
		b.ballVel.x = -BVEL;

		if (b.y() <= c.y())
			b.ballVel.y = -BVEL;
		else
			b.ballVel.y = BVEL;
	}
	else
		return;

}

//***********checking if ball colided with wall
//***********if so, point is taken from player
//***********and counter (OCREDIT) reset by 1.
void colideWithWall(Block& a, Ball& b, Block& c){
	if (b.leftSide() < 0)
		a.scr = bitset<7>(a.scr.to_ulong() - 1);
	else if (b.rightSide() > WWIDTH)
		c.scr = bitset<7>(c.scr.to_ulong() - 1);
	return;
}

int main(){
	sf::RenderWindow wind1({ WWIDTH, WHEIGHT }, "Pong!");
	wind1.setFramerateLimit(60);

	//*********instantiating one ball
	Ball b(WWIDTH / 2.f, WHEIGHT / 2.f);

	//**********1 block per side to play pong.
	//**********If ball hits either of the walls behind the 
	//**********blocks, the player looses a point:
	//**********starting from CREDIT = 100 until 0.
	Block bL(45, WHEIGHT / 2, 'L');
	Block bR(WWIDTH - 45.f, WHEIGHT / 2.f, 'R');
	
	//***********to store end game signal string size
	sf::FloatRect stringSize;

	//**********To draw text with score.
	//**********Score is a countown from 100
	//**********substracted 1 each time the ball
	//**********hits the wall behind paddle.
	sf::Font font;
	if (!font.loadFromFile("c:/users/home1/documents/visual studio 2013/Projects/pong/include/fonts/ArchitectsDaughter.ttf"))
		exit(0);

	sf::Text scoreL, scoreR;
	scoreL.setOrigin({ 0, 0 });
	scoreL.setPosition({ 2.f, 2.f });
	scoreL.setFont(font);
	scoreL.setColor(sf::Color::Red);
//	scoreL.setStyle(sf::Text::Bold);

	scoreR.setOrigin({ 0, 0 });
	scoreR.setPosition({ WWIDTH - 50.f, WHEIGHT - 50.f});
	scoreR.setFont(font);
	scoreR.setColor(sf::Color::Red);
//	scoreR.setStyle(sf::Text::Bold);
	//***********

	while (wind1.isOpen()){
		wind1.clear(sf::Color::Black);

		sf::Event ev;

		while (wind1.pollEvent(ev)){

			if (ev.type == sf::Event::Closed)
				//***********if escape pressed: game over.
					wind1.close();

			//***********if n pressed, game re-starts.
			//***********score is reset;
			//***********position of score on screen is reset:
			//***********score moves to center signaling loss of game.
				else if (ev.key.code == sf::Keyboard::N){
					BVEL = 6.f;
					bL.scr = bitset<7>(2);
					bR.scr = bitset<7>(2);
					scoreL.setPosition({ 2.f, 2.f });
					scoreR.setPosition({ WWIDTH - 50.f, WHEIGHT - 50.f });
				}
		}

		b.update();
		bL.update();
		bR.update();

		colide(bL, b, bR);
		colideWithWall(bL, b, bR);
		
		scoreL.setString(to_string((bL.scr).to_ulong()));
		scoreR.setString(to_string((bR.scr).to_ulong()));

		//***********if score is 0 game is over.
		if (bL.scr.to_ulong() == 0){
			BVEL = 0.f;
			b.ballShape.setPosition(WWIDTH / 2.f, WHEIGHT / 2.f);
			scoreL.setString("Left Side: YOU Lost :_(");
			stringSize = scoreL.getGlobalBounds();
			scoreL.setPosition({ (WWIDTH / 2.f) - (stringSize.width / 2), (WHEIGHT / 2.f) - 80 });
		}
		//***********if score is 0 game is over.
		else if (bR.scr.to_ulong() == 0){
			BVEL = 0.f;
			b.ballShape.setPosition(WWIDTH / 2.f, WHEIGHT / 2.f);
			scoreR.setString("Right Side: YOU Lost :_(");
			stringSize = scoreR.getGlobalBounds();
			scoreR.setPosition({ (WWIDTH / 2.f) - (stringSize.width / 2), (WHEIGHT / 2.f) - 80 });

		}

		wind1.draw(b.ballShape);
		wind1.draw(bL.recShape);
		wind1.draw(bR.recShape);
		wind1.draw(scoreL);
		wind1.draw(scoreR);

		wind1.display();

	}
	
	return 0;
}